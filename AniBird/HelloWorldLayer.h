//
//  HelloWorldLayer.h
//  AniBird
//
//  Created by Daniel Burke on 1/23/13.
//  Copyright Daniel Burke 2013. All rights reserved.
//


#import <GameKit/GameKit.h>
#import "cocos2d.h"
#import "Bird.h"


@interface HelloWorldLayer : CCLayerColor <GKAchievementViewControllerDelegate, GKLeaderboardViewControllerDelegate>
{
    Bird *_bird;
    NSMutableArray *_birds;
    NSMutableArray *_clouds;
    BOOL _moving;
    
    CGPoint firstTouch;
    CGPoint lastTouch;
}

@property (nonatomic, retain) Bird *bird;
@property (nonatomic, retain) NSMutableArray *birds;
@property (nonatomic, retain) NSMutableArray *clouds;
@property (nonatomic, retain) CCAction *moveAction;
@property (nonatomic, retain) CCSprite *hitArea1;
@property (nonatomic, retain) CCSprite *hitArea2;
@property (nonatomic) int safeBirds;
@property (nonatomic) int lostBirds;

+(CCScene *) scene;
-(void)dropBird;
-(void)addCloud:(CGPoint)location andLocationTwo:(CGPoint)location2;

@end
