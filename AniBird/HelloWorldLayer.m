//
//  HelloWorldLayer.m
//  AniBird
//
//  Created by Daniel Burke on 1/23/13.
//  Copyright Daniel Burke 2013. All rights reserved.
//

#import "HelloWorldLayer.h"
#import "AppDelegate.h"
#import "Bird.h"

#pragma mark - HelloWorldLayer

@implementation HelloWorldLayer

+(CCScene *) scene
{
	CCScene *scene = [CCScene node];
	HelloWorldLayer *layer = [HelloWorldLayer node];
	[scene addChild: layer];
	return scene;
}

-(id) init {
    if((self = [super initWithColor:ccc4(255, 255, 255, 255)])) {
        
        [self setTouchEnabled:YES];
        [[[CCDirector sharedDirector] view] setMultipleTouchEnabled:YES];
        CGSize winSize = [CCDirector sharedDirector].winSize;
        
        CCSprite *bg = [CCSprite spriteWithFile:@"bgV_sky.png"];
        [bg setPosition:ccp(160, 284)];
        bg.rotation = 90;
        
        CCSprite *sun = [CCSprite spriteWithFile:@"bgV_sun.png"];
        [sun setPosition:ccp(200, 100)];
        [self addChild:sun z:1];
        
        CCSprite *landscape = [CCSprite spriteWithFile:@"bgV_hills.png"];
        [landscape setPosition:ccp(278, 74)];
        [self addChild:landscape z:2];
        
        CCSprite *nest1 = [CCSprite spriteWithFile:@"nest1.png"];
        [nest1 setPosition:ccp(winSize.width - 40, 80)];
        [self addChild:nest1 z:5];
        
        CCSprite *nest2 = [CCSprite spriteWithFile:@"nest2.png"];
        [nest2 setPosition:ccp(40, 40)];
        [self addChild:nest2 z:5];
        
        CCSprite *nest3 = [CCSprite spriteWithFile:@"nest3.png"];
        [nest3 setPosition:ccp(winSize.width - 60, 180)];
        [self addChild:nest3 z:5];
        
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile: @"bird_green.plist"];
        CCSpriteBatchNode *spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"bird_green.png"];
        
        NSMutableArray *flutterAnimFrames = [NSMutableArray array];
        for(int i = 1; i <= 2; ++i) {
            [flutterAnimFrames addObject:
             [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
              [NSString stringWithFormat:@"Bird%d.png", i]]];
        }
        
        CCSprite *bird3 = [CCSprite spriteWithSpriteFrameName:@"Bird1.png"];;
        [bird3 setPosition:ccp(winSize.width - 55, 210)];
        [self addChild:bird3 z:4];
        
        CCSprite *nest4 = [CCSprite spriteWithFile:@"nest4.png"];
        [nest4 setPosition:ccp(60, 200)];
        [self addChild:nest4 z:5];
        
        CCSprite *bird4 = [CCSprite spriteWithSpriteFrameName:@"Bird1.png"];;
        [bird4 setPosition:ccp(78, 235)];
        [self addChild:bird4 z:4];
        
        _birds = [[NSMutableArray alloc] init];
        _clouds = [[NSMutableArray alloc] init];
        
        //UPDATE UI TO CHECK TO SEE IF A BIRD HAS HID A CLOUD
        //OR IF A BIRD HAS HIT A NEST
        [self schedule:@selector(update:)];
        
        [self dropBird];
        
        
    }
    return self;
}

-(void)dropBird{
    _bird = nil;
    
    //ADD BIRD CLASS
//    if (arc4random() % 2 == 0) {
//        _bird = [[Bird alloc] initWithBirdColor:@"red"];
//    } else {
//        _bird = [[Bird alloc] initWithBirdColor:@"green"];
//    }

    _bird = [[Bird alloc] initWithBirdColor:@"green"];
    
    CGSize winSize = [CCDirector sharedDirector].winSize;
    int minX = 100;
    int maxX = winSize.width - 100; //KEEP THE BIRDS TOWARD THE CENTER
    int rangeX = maxX - minX;
    int actualX = (arc4random() % rangeX) + minX;
    
    _bird.position = ccp(actualX, winSize.height + _bird.contentSize.height);
    [self addChild:_bird z:3];
    
    _bird.tag = 1;
    [_birds addObject:_bird]; //KEEP TRACK OF BIRDS
    
    int minDuration = 2.0;
    int maxDuration = 4.0;
    int rangeDuration = maxDuration - minDuration;
    int actualDuration = (arc4random() % rangeDuration) + minDuration;
    
    
    CCMoveTo * actionMove = [CCMoveTo actionWithDuration:actualDuration
                                                position:ccp(actualX, -40)];
    CCCallBlockN * actionMoveDone = [CCCallBlockN actionWithBlock:^(CCNode *node) {
        [node removeFromParentAndCleanup:YES];
        _lostBirds++;
        
        // REMOVE FROM ARRAY WHEN OFF THE SCREEN
        [_birds removeObject:node];
        [self dropBird];
    }];
    [_bird runAction:[CCSequence actions:actionMove, actionMoveDone, nil]];
}

-(void)addCloud:(CGPoint)location1 andLocationTwo:(CGPoint)location2{
    CCSprite *cloud = [CCSprite spriteWithFile:@"cloud.png"];
    
    //EASIER TO WORK WITH INTS
    int touch1X = location1.x;
    int touch2X = location2.x;
    
    int touch1Y = location1.y;
    int touch2Y = location2.y;
    
    //FIND THE LEFT-MOST TOUCH
    int minX = (touch1X < touch2X) ? touch1X : touch2X;
    int maxX = (minX == touch1X) ? touch2X : touch1X;
    
    //FIND THE BOTTOM-MOST TOUCH
    int minY = (touch1Y < touch2Y) ? touch1Y : touch2Y;
    int maxY = (minY == touch1Y) ? touch2Y : touch1Y;
    
    int touchXDiff = maxX - minX;
    int touchYDiff = maxY - minY;
    
    [cloud setPosition:ccp(touchXDiff/2 + minX, touchYDiff/2 + minY)];
    
    cloud.tag = 1;
    [_clouds addObject:cloud];
    
    [self addChild:cloud z:10];
}

-(void)moveCloud:(CCSprite*)cloud withTouchOne:(CGPoint)location1 andTouchTwo:(CGPoint)location2{
    //EASIER TO WORK WITH INTS
    int touch1X = location1.x;
    int touch2X = location2.x;
    
    int touch1Y = location1.y;
    int touch2Y = location2.y;
    
    //FIND THE LEFT-MOST TOUCH
    int minX = (touch1X < touch2X) ? touch1X : touch2X;
    int maxX = (minX == touch1X) ? touch2X : touch1X;
    
    //FIND THE BOTTOM-MOST TOUCH
    int minY = (touch1Y < touch2Y) ? touch1Y : touch2Y;
    int maxY = (minY == touch1Y) ? touch2Y : touch1Y;
    
    int touchXDiff = maxX - minX;
    int touchYDiff = maxY - minY;
    
    [cloud setPosition:ccp(touchXDiff/2 + minX, touchYDiff/2 + minY)];
    
    //ROTATE CLOUD AS IT MOVES
    
    int offDiffX = touch1X - touch2X;
    int offDiffY = touch1Y - touch2Y;
    
    float angleRadians = atanf((float)offDiffY / (float)offDiffX);
    float angleDegrees = CC_RADIANS_TO_DEGREES(angleRadians);
    float cloudAngle = -1 * angleDegrees;
    
    [cloud setRotation:cloudAngle];
}

-(void)ccTouchesBegan:(NSSet*)touches withEvent:(UIEvent*)event {

    NSSet *allTouches = [event allTouches];
    
    if([allTouches count] == 2){
        UITouch * touch1 = [[allTouches allObjects] objectAtIndex:0];
        CGPoint location1 = [touch1 locationInView: [touch1 view]];
        location1 = [[CCDirector sharedDirector] convertToGL:location1];
        
        UITouch * touch2 = [[allTouches allObjects] objectAtIndex:1];
        CGPoint location2 = [touch2 locationInView: [touch2 view]];
        location2 = [[CCDirector sharedDirector] convertToGL:location2];
        
        if([_clouds count] == 0){
            [self addCloud:location1 andLocationTwo:location2];
            
            
            _hitArea1 = [CCSprite spriteWithFile:@"hit_area.png"];
            [_hitArea1 setPosition:ccp(location1.x, location1.y)];
            [self addChild:_hitArea1 z:11];
            
            _hitArea2 = [CCSprite spriteWithFile:@"hit_area.png"];
            [_hitArea2 setPosition:ccp(location2.x, location2.y)];
            [self addChild:_hitArea2 z:11];
        }
    }
    
}

-(void)ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    
    NSSet *allTouches = [event allTouches];
    if([allTouches count] == 2){
        UITouch * touch1 = [[allTouches allObjects] objectAtIndex:0];
        CGPoint location1 = [touch1 locationInView: [touch1 view]];
        location1 = [[CCDirector sharedDirector] convertToGL:location1];
        
        UITouch * touch2 = [[allTouches allObjects] objectAtIndex:1];
        CGPoint location2 = [touch2 locationInView: [touch2 view]];
        location2 = [[CCDirector sharedDirector] convertToGL:location2];
        
        [self moveCloud:[_clouds objectAtIndex:0] withTouchOne:location1 andTouchTwo:location2];
        [_hitArea1 setPosition:ccp(location1.x, location1.y)];
        [_hitArea2 setPosition:ccp(location2.x, location2.y)];
    }
}

-(void)update:(ccTime)delta{
    if ([_clouds count] > 0) {
        //CHECK IF THE BIRD AND CLOUD HAVE COLLIDED
        CCSprite *cloud = (CCSprite *)[_clouds objectAtIndex:0];
        float cloudAngle = cloud.rotation;
        
        if(CGRectIntersectsRect(_bird.boundingBox, cloud.boundingBox)){
            [_bird bounce:cloudAngle];
        }
    }
}

- (void) dealloc{
    _bird = nil;
	[super dealloc];
}



#pragma mark GameKit delegate

-(void) achievementViewControllerDidFinish:(GKAchievementViewController *)viewController
{
	AppController *app = (AppController*) [[UIApplication sharedApplication] delegate];
	[[app navController] dismissModalViewControllerAnimated:YES];
}

-(void) leaderboardViewControllerDidFinish:(GKLeaderboardViewController *)viewController
{
	AppController *app = (AppController*) [[UIApplication sharedApplication] delegate];
	[[app navController] dismissModalViewControllerAnimated:YES];
}
@end
