//
//  Nest.h
//  AniBird
//
//  Created by Daniel Burke on 1/27/13.
//  Copyright 2013 Daniel Burke. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface Nest : CCSprite {
    
}

@property (nonatomic, assign) NSString *birdColor;

-(id)initWithBirdColor:(NSString *)color;

@end
