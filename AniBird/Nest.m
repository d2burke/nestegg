//
//  Nest.m
//  AniBird
//
//  Created by Daniel Burke on 1/27/13.
//  Copyright 2013 Daniel Burke. All rights reserved.
//

#import "Nest.h"


@implementation Nest

-(id)initWithBirdColor:(NSString *)color{
    if((self = [super init])){
        if([color isEqualToString:@"green"] || [color isEqualToString:@"red"]){
            _birdColor = color;
        }
        else{
            _birdColor = @"green";
        }
        
        CGSize winSize = [CCDirector sharedDirector].winSize;
        
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile: @"bird_green.plist"];
        CCSpriteBatchNode *spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"bird_green.png"];
        
        NSMutableArray *flutterAnimFrames = [NSMutableArray array];
        for(int i = 1; i <= 2; ++i) {
            [flutterAnimFrames addObject:
             [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
              [NSString stringWithFormat:@"Bird%d.png", i]]];
        }
        
        CCSprite *bird = [CCSprite spriteWithSpriteFrameName:@"Bird1.png"];;
        [bird setPosition:ccp(winSize.width - 55, 210)];
        [self addChild:bird z:4];
        
        CCSprite *nest3 = [CCSprite spriteWithFile:@"nest3.png"];
        [nest3 setPosition:ccp(winSize.width - 60, 180)];
        [self addChild:nest3 z:5];
    }
    
    return self;
}

@end
